package br.com.prova.estacionamento.repository;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.prova.estacionamento.entity.Preco;


/**
 * @author raphael
 * Acesso ao Banco
 */
@Stateless
public class PrecoRepositoryImpl implements PrecoRepository{

	@PersistenceContext(unitName="EstacionamentoPU")
	private EntityManager em;

	@Override
	public Preco buscaVeiculo(String marca, String modelo, String placa) {
		String hql = "SELECT p "
				+ "			FROM Preco p "
				+ "	   WHERE p.marca=:marca"
				+ "		AND  p.modelo=:modelo";
		try{
			return em.createQuery(hql, Preco.class)
			.setParameter("marca", marca.toUpperCase())
			.setParameter("modelo", modelo.toUpperCase())
			.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}
	

}
